package com.realdolmen.teamPHP.controllers;

import com.realdolmen.teamPHP.dtos.ProductDTO;
import com.realdolmen.teamPHP.facade.ProductFacade;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Named
@SessionScoped
public class ProductController implements Serializable {
    private List<ProductDTO> productDTOS;
    private ProductDTO productDTO;


    private  String type;
    private String id;

    @Inject
    private ProductFacade productFacade;

    public ProductController() {
    }

    
    @PostConstruct
    public void init() {
        System.out.println("In init method");
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        type = params.get("type");
        System.out.println(type);
    }

    public List<ProductDTO> getAllProducts(String string) {
        productDTOS = productFacade.findAll(string);
        return productDTOS;
    }
    public ProductDTO getProduct (String productType,String id) {
        productDTO = productFacade.findProduct(productType,Long.parseLong(id));
        return productDTO;
    }

    public void update(String P_name, Double P_price, LocalDate P_manufactory, LocalDate P_releaseDate, Integer P_warranty, Boolean p_wireless){

        System.out.println(P_name + P_price + P_manufactory + P_releaseDate + P_warranty +p_wireless);

    }

    public void setProductDTOS(List<ProductDTO> productDTOS) {
        this.productDTOS = productDTOS;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
