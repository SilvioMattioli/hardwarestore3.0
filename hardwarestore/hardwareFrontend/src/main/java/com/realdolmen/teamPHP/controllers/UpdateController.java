package com.realdolmen.teamPHP.controllers;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class UpdateController implements Serializable {
    private Boolean showInput ;

    public UpdateController(){

    }
    @PostConstruct
    public void init() {
        System.out.println("In init method van UpdateController");
        showInput = false;
    }


    public Boolean currentState(){
        System.out.println("currently in currentState");
       return  showInput;
    }
    public void setCurrentState(){

        if(showInput){
            System.out.println("current state is true");
            showInput = false;
            //return currentState();
        }
        else {
            showInput = true;
            //return  currentState();
        }
    }


    public boolean isShowInput() {
        return showInput;
    }

    public void setShowInput(boolean showInput) {
        this.showInput = showInput;
    }
}