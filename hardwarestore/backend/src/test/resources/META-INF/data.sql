
insert into shopbasket(id) values(1000);
insert into shopbasket(id) values(2000);
insert into shopbasket(id) values(3000);
insert into shopbasket(id) values(4000);
insert into shopbasket(id) values(5000);
insert into shopbasket(id) values(6000);
insert into shopbasket(id) values(7000);
insert into shopbasket(id) values(8000);
insert into shopbasket(id) values(9000);
insert into shopbasket(id) values(10000);

insert into login(id,username,password,isAdmin) values(1000,'Jolly Ranger','jejeosjeksd',true);
insert into login(id,username,password,isAdmin) values(2000,'Pink Kisses','fjfksoezs',false);
insert into login(id,username,password,isAdmin) values(3000,'Gobstopper Heart Breakers','dkeielsdklsdke',true);
insert into login(id,username,password,isAdmin) values(4000,'Kahlua Liquor','jkejeljdlmldkze',false);
insert into login(id,username,password,isAdmin) values(5000,'Gummy Bears','mpoepododd',false);
insert into login(id,username,password,isAdmin) values(6000,'Sweethearts','dsdkdjmdkmd',false);

insert into user_account(id,name,basket_id) values(1000,'Jolly Ranger',1000);
insert into user_account(id,name,basket_id) values(2000,'Pink Kisses',2000);
insert into user_account(id,name,basket_id) values(3000,'Gobstopper Heart Breakers',3000);
insert into user_account(id,name,basket_id) values(4000,'Kahlua Liquor',4000);
insert into user_account(id,name,basket_id) values(5000,'Gummy Bears',5000);
insert into user_account(id,name,basket_id) values(6000,'Sweethearts',6000);


insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (1000,"manufactory1","Harddrive Ultra",7.00,"2019-10-6",3,10);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (2000,"manufactory2","Harddrive Master",10.00,"2017-10-20",3,12);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (3000,"manufactory3","Harddrive Noob",5.00,"2009-7-6",3,3);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (4000,"manufactory4","Harddrive Pink",10.00,"2001-3-6",7,33);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (17000,"manufactory1","Harddrive Ultra1",7.00,"2019-10-6",3,10);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (18000,"manufactory2","Harddrive Master2",10.00,"2017-10-20",3,12);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (19000,"manufactory3","Harddrive Noob3",5.00,"2009-7-6",3,3);
insert into hdd(id, manufactory, name, price, releaseDate, warranty, rpm)values (20000,"manufactory4","Harddrive Pink4",10.00,"2001-3-6",7,33);


insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (5000,"manufactory2","Keyboard Master",10.00,"2017-10-20",3,true);
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (6000,"manufactory1","Keyboard Ultra",7.00,"2017-10-19",2,false);
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (7000,"manufactory3","Keyboard Pink",6.00,"2017-9-20",8,false);
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (8000,"manufactory4","Harddrive Noob",8.00,"2017-3-12",6,true);
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (21000,"manufactory2","Keyboard Master",10.00,"2017-10-20",4,true );
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (22000,"manufactory1","Keyboard Ultra",7.00,"2017-10-19",3,false);
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (23000,"manufactory3","Keyboard Pink",6.00,"2017-9-20",4,true);
insert into keyboard(id, manufactory, name, price, releaseDate, warranty,isWireless)values (24000,"manufactory4","Harddrive Noob",8.00,"2017-3-12",1,true);


insert into mouse(id, manufactory, name, price, releaseDate, warranty,isWireless)values (9000,"manufactory2","Mouse Master",10.00,"2017-10-20",1,true);
insert into mouse(id, manufactory, name, price, releaseDate, warranty,isWireless)values (10000,"manufactory1","Mouse Ultra",7.00,"2017-10-19",6,false);
insert into mouse(id, manufactory, name, price, releaseDate, warranty,isWireless)values (11000,"manufactory3","Mouse Pink",6.00,"2017-9-20",2,true);
insert into mouse(id, manufactory, name, price, releaseDate, warranty,isWireless)values (12000,"manufactory4","Mouse Noob",8.00,"2017-3-12",4,false);

insert into screen(id, manufactory, name, price, releaseDate, warranty)values (13000,"manufactory2","Screen Master",10.00,"2017-10-20",1);
insert into screen(id, manufactory, name, price, releaseDate, warranty)values (14000,"manufactory1","Screen Ultra",7.00,"2017-10-19",1);
insert into screen(id, manufactory, name, price, releaseDate, warranty)values (15000,"manufactory3","Screen Pink",6.00,"2017-9-20",3);
insert into screen(id, manufactory, name, price, releaseDate, warranty)values (16000,"manufactory4","Screen Noob",8.00,"2017-3-12",3);


insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (25000,"manufactory2","ssd Master",10.00,"2017-10-20",3);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (26000,"manufactory1","ssd Ultra",7.00,"2017-10-19",6);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (27000,"manufactory3","ssd Pink",6.00,"2017-9-20",2);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (28000,"manufactory4","ssd Noob",8.00,"2017-3-12",3);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (29000,"manufactory2","ssd Master",10.00,"2017-10-20",5);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (30000,"manufactory1","ssd Ultra",7.00,"2017-10-19",3);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (31000,"manufactory3","ssd Pink",6.00,"2017-9-20",2);
insert into ssd(id, manufactory, name, price, releaseDate, warranty)values (32000,"manufactory4","ssd Noob",8.00,"2017-3-12",4);

select  * from keyboard



