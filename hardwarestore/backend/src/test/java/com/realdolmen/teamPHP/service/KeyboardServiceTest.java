package com.realdolmen.teamPHP.service;

import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;
import com.realdolmen.teamPHP.services.KeyboardService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class KeyboardServiceTest {

   @Inject
    private KeyboardService keyboardService;

    @InjectMocks
    private KeyboardRepo keyboardRepo;

    @Before
    public void init(){
        keyboardService=new KeyboardService();
       // keyboardRepo = Mockito.mock(KeyboardRepo.class);




    }


    @Test
    public void findAllKeyboardTest(){
        //init data
        //List<Keyboard> kBoards=new ArrayList<>(Arrays.asList(new Keyboard()));
        //when(keyboardRepo.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Keyboard())));
        List<Keyboard> kBoards= new ArrayList<>();
        when(keyboardRepo.findAll()).thenReturn(kBoards);

        //doe de test
        List<Keyboard> kResults ;

        kResults= keyboardService.findAllKeyBService();

        //veriffy
        assertEquals(kBoards,kResults);
        verify(keyboardRepo,times(1)).findAll();
        verifyNoMoreInteractions(keyboardRepo);

    }

    /*
    @Test
    public void findKeyboarbById(){
        Keyboard kBoards = new Keyboard();
        when(keyboardRepo.findById(7000L)).thenReturn(kBoards);

        Keyboard kResult = keyboardService.f(7000L);

        assertEquals(kBoards,kResult);
        assertEquals(kBoards.getName(),kResult.getName());
        assertEquals(7000L,kResult.getId().longValue());
        verify(keyboardRepo,times(1)).findById(7000L);
        verifyNoMoreInteractions(keyboardRepo);

    }
*/
    @Ignore
    @Test
    public void filterOnWirelessKeyBTest(){
        List<Keyboard> kBoards= new ArrayList<>();
        when(keyboardRepo.filterOnWirless(true)).thenReturn(kBoards);

        List<Keyboard> kResults = keyboardService.filterOnWirelessService(true);

        assertEquals(kBoards,kResults);
        assertEquals(kBoards.get(0).getWireless(),kResults.get(0).getWireless());
        assertTrue(kResults.get(1).getWireless());
        verify(keyboardRepo,times(1)).filterOnWirless(true);
        verifyNoMoreInteractions(keyboardRepo);

    }
    @Ignore
    @Test
    public void filterPriceBtweenKeyBTest(){
        List<Keyboard> kBoards= new ArrayList<>();
        when(keyboardRepo.filterPriceBtween(10.00,20.00)).thenReturn(kBoards);

        List<Keyboard> kResults = keyboardService.filterPriceBtweenService(10.00,20.00);

        assertEquals(kBoards,kResults);
        assertTrue(kResults.get(0).getPrice()>10.00 && kResults.get(0).getPrice()<20.00);
        verify(keyboardRepo,times(1)).filterPriceBtween(10.00,20.00);
        verifyNoMoreInteractions(keyboardRepo);

    }
    @Ignore
    @Test
    public void filterManufactuaryTest(){
        List<Keyboard> kBoards= new ArrayList<>();
        when(keyboardRepo.filterManufactuary("manufactory3")).thenReturn(kBoards);

        List<Keyboard> kResults = keyboardService.filterManufactuaryService("manufactory3");

        assertEquals(kBoards,kResults);
        assertEquals("manufactory3",kResults.get(0).getManufactory());
        verify(keyboardRepo,times(1)).filterManufactuary("manufactory3");
        verifyNoMoreInteractions(keyboardRepo);

    }
    @Ignore
    @Test
    public void filterPriceAndWirelessTest(){
        List<Keyboard> kBoards= new ArrayList<>();
        when(keyboardRepo.filterPriceAndWireless(10.00,20.00,true)).thenReturn(kBoards);

        List<Keyboard> kResults = keyboardService.filterPriceAndWirelessService(10.00,20.00,true);

        assertEquals(kBoards,kResults);
        assertTrue(kResults.get(0).getPrice()>10.00 && kResults.get(0).getPrice()<20.00);
        assertTrue(kResults.get(0).getWireless());
        verify(keyboardRepo,times(1)).filterPriceAndWireless(10.00,20.00,true);
        verifyNoMoreInteractions(keyboardRepo);

    }
    @Ignore
    @Test
    public void filterNameLikeTest(){
        List<Keyboard> kBoards= new ArrayList<>();
        when(keyboardRepo.filterNameLike("Hard")).thenReturn(kBoards);

        List<Keyboard> kResults = keyboardService.filterNameLikeService("Hard");

        assertEquals(kBoards,kResults);
        assertTrue(kResults.get(0).getName().contains("Hard"));
        verify(keyboardRepo,times(1)).filterNameLike("Hard");
        verifyNoMoreInteractions(keyboardRepo);

    }
}
