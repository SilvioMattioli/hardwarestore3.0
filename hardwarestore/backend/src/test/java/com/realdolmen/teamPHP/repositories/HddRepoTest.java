package com.realdolmen.teamPHP.repositories;




import com.realdolmen.teamPHP.domain.HDD;
import com.realdolmen.teamPHP.domain.ShopBasket;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.constraints.Null;
import org.junit.After;
import org.junit.AfterClass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class HddRepoTest {

    private HddRepo hddrepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;


    @BeforeClass
    public static void initCLass()
    {
       emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        em = emf.createEntityManager();
        hddrepo = new HddRepo(em);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
    }
    @Test
    public void findHDDByIdTest() {
        HDD hdd = hddrepo.findById(4000L);
        assertNotNull(hdd);
        assertEquals(hdd.getName(),"Harddrive Pink");
    }

    /**
     *
     */
    @Test
    public void addHddDriveTest() {
        HDD hdd = new HDD();
        hdd.setManufactory("manufactory5");
        hdd.setName("Harddrive Noob");
        hdd.setPrice(7.50);
        LocalDate date1= LocalDate.of(2014, Month.APRIL,1);
        hdd.setReleaseDate(date1);
        System.out.println(hdd.getReleaseDate());
        hdd.setWarranty(2);
        ShopBasket sb = new ShopBasket();
        sb.setId(5555L);
        hdd.setShopBasket(sb);
        hdd.setRpm(3L);
        hddrepo.save(hdd);

        System.out.println("adding "+hdd.getClass()+ "To Database");

        HDD hdd1=em.find(HDD.class,hdd.getId());
        //assertEquals(5555,hdd1);
        assertEquals("Harddrive Noob",hdd1.getName());
        assertEquals("manufactory5",hdd1.getManufactory());
        assertEquals(date1,hdd1.getReleaseDate());
        assertEquals(2,(int)hdd1.getWarranty());
        //assertEquals(5555,(long)hdd1.getShopBasket().getId());
        assertEquals(3,(long)hdd1.getRpm());

    }
    @Test
    public  void deleteHddTest(){
        hddrepo.delete(20000L);
        assertEquals(null,em.find(HDD.class,20000L));
    }
    @Test
    public void updateHddTest(){
        HDD hdd = em.find(HDD.class,1000l);
        hddrepo.begin();
        em.detach(hdd);
        hdd.setRpm(500L);
        em.flush();
        em.clear();
        hddrepo.update(hdd);
        assertEquals(500,(long)em.find(HDD.class,1000L).getRpm());
        //assertNotNull(em.find(HDD.class,1000L));
    }
    @Test
    public void filterHddRpmTest(){
        List<HDD> hdds = hddrepo.filterOnRpm(5L);
        for (HDD hd:hdds){
            System.out.println(hd.getPrice());
        }
        assertTrue(hdds.size() >0);
    }
    @Test
    public void filterHddOnPriceTest(){
        List<HDD> hdds = hddrepo.filterOnPrice(5.00);
        for (HDD hd:hdds){
            System.out.println(hd.toString());
        }
        assertTrue(hdds.size() >0);
    }


    @After
    public void exit() {
        hddrepo.close();
    }

    @AfterClass
    public static void exitClass(){
        emf.close();
    }


}
