package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Login;
import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.domain.UserAccount;
import com.realdolmen.teamPHP.repositories.LoginRepo;
import com.realdolmen.teamPHP.repositories.ShopbasketRepo;
import com.realdolmen.teamPHP.repositories.UserAccRepo;
import com.sun.xml.bind.v2.TODO;
import org.junit.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
public class UserAccRepoTest {
    private UserAccRepo userAccRepo;
    private ShopbasketRepo shopbasketRepo;
    private LoginRepo loginRepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;

    @BeforeClass
    public static void initCLass()
    {
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        userAccRepo = new UserAccRepo(em);
        shopbasketRepo = new ShopbasketRepo(em);
        loginRepo= new LoginRepo(em);

        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
    }

    @Test
    public void findUserByIdTest() {
        UserAccount user = userAccRepo.findById(1000L);
        assertNotNull(user);
        assertEquals(user.getName(),"Jolly Ranger");
    }
    //Error
    /*@Test
    public void addUserTest() throws ParseException {


        UserAccount user = new UserAccount();
        user.setName("Willy Wanka");
        ShopBasket sb = new ShopBasket();
        sb.setId(5577L);
        shopbasketRepo.save(sb);
        user.setShopbasket(sb);
        Login lg=new Login();
        lg.setId(55L);
        loginRepo.save(lg);
        user.setShopbasket(sb);
       // user.setLogin(lg);

        userAccRepo.save(user);
        System.out.println("adding "+user.getClass()+ "To Database");
        UserAccount user1=em.find(UserAccount.class,user.getId());
        assertEquals("Willy Wanka",user1.getName());

    }*/

    @Test
    public  void deleteKeyboardTest(){
        userAccRepo.delete(6000L);
        assertEquals(null,em.find(UserAccount.class,6000L));
    }

    @Test
    public void updateKeyboardTest(){
        UserAccount userAccount = em.find(UserAccount.class,4000l);
        userAccRepo.begin();
        em.detach(userAccount);
        userAccount.setName("Simba Lion");
        em.flush();
        em.clear();
        userAccRepo.update(userAccount);
        assertEquals("Simba Lion",em.find(UserAccount.class,4000L).getName());
        //assertNotNull(em.find(HDD.class,1000L));
    }
    @Test
    public void filterNameLikeTest(){
        List<UserAccount> users = userAccRepo.filterNameLike("Kis");

        users.stream().forEach(us -> System.out.println(us.toString()));
        assertTrue(users.size() >0);
    }

    @After
    public void exit() {
        userAccRepo.close();
    }

    @AfterClass
    public static void exitClass(){
        if(emf !=null) {
            emf.close();
        }
    }


}
