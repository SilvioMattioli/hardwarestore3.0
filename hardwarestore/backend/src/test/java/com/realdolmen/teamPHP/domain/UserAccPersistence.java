package com.realdolmen.teamPHP.domain;

import com.realdolmen.teamPHP.domain.UserAccount;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserAccPersistence extends AbstractPersistence {

        @Test
        public void PersistUserAcc(){
            UserAccount userA = new UserAccount();
            //userA.setId(1L);
            userA.setName("Eerste User");
            em.persist(userA);
            assertNotNull(userA.getId());
            assertEquals(userA.getName(),"Eerste User");


        }

        @Test
        public void LoadUserAcc(){
            UserAccount userA=(em.find(UserAccount.class,2000L));
            assertNotNull(userA);
            assertEquals(userA.getName(),"Pink Kisses");
        }
}
