package com.realdolmen.teamPHP.repositories;
import com.realdolmen.teamPHP.domain.SSD;
import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.repositories.SsdRepo;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class SsdRepoTest {

    private SsdRepo ssdrepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;


    @BeforeClass
    public static void initCLass()
    {
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        ssdrepo = new SsdRepo(em);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
    }
    @Test
    public void findSSDByIdTest() {
        SSD ssd = ssdrepo.findById(28000L);
        assertNotNull(ssd);
        assertEquals(ssd.getName(),"ssd Noob");
    }
    @Test
    public void addSSDDriveTest() throws ParseException {
        SSD ssd = new SSD();
        ssd.setManufactory("manufactory5");
        ssd.setName("Harddrive Noob");
        ssd.setPrice(7.50);
        LocalDate date1= LocalDate.of(2014, Month.APRIL,1);
        ssd.setReleaseDate(date1);
        System.out.println(ssd.getReleaseDate());
        ssd.setWarranty(2);
        ShopBasket sb = new ShopBasket();
        sb.setId(5555L);
        ssd.setShopBasket(sb);
        ssdrepo.save(ssd);

        System.out.println("adding "+ssd.getClass()+ "To Database");

        SSD ssd1=em.find(SSD.class,ssd.getId());
        //assertEquals(5555,ssd1);
        assertEquals("Harddrive Noob",ssd1.getName());
        assertEquals("manufactory5",ssd1.getManufactory());
        assertEquals(date1,ssd1.getReleaseDate());
        assertEquals(2,(int)ssd1.getWarranty());
        //assertEquals(5555,(long)ssd1.getShopBasket().getId());

    }
    @Test
    public  void deleteHddTest(){
        ssdrepo.delete(25000L);
        assertEquals(null,em.find(SSD.class,25000L));
    }
    @Test
    public void updateHddTest(){
        SSD ssd = em.find(SSD.class,26000L);
        ssdrepo.begin();
        em.detach(ssd);
        ssd.setPrice(599.00);
        em.flush();
        em.clear();
        ssdrepo.update(ssd);
        assertEquals(599.00,em.find(SSD.class,26000L).getPrice(), 0b0);
    }
    @Test
    public void filterSsdOnPriceTest(){
        List<SSD> ssds = ssdrepo.filterOnPrice(8.00);
        for (SSD ssd:ssds){
            System.out.println(ssd.toString());
        }
        assertTrue(ssds.size() >0);
    }



    @After
    public void exit() {
        ssdrepo.close();
    }

    @AfterClass
    public static void exitClass(){
        emf.close();
    }


}
