package com.realdolmen.teamPHP.builders;

import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.dtos.ProductDTO;

import java.time.LocalDate;

public class ProductDTObuilder {

    private ProductDTO productDTO;

    private Long id;
    private Double price;
    private String name;
    private String manufactory;
    private LocalDate releaseDate;
    //wrm is warranty een string het is toch gebaseerd op tijd ?
    private Integer warranty;
    private ShopBasket shopbasket;
    private Long rpm;
    private Boolean isWireless ;



    public ProductDTObuilder() {
        productDTO = new ProductDTO();
    }



    public ProductDTObuilder setId(Long id) {
        this.productDTO.setId(id);
        return this;
    }

    public ProductDTObuilder setPrice(Double price) {
        this.productDTO.setPrice(price);
        return this;
    }

    public ProductDTObuilder setName(String name) {
        this.productDTO.setName(name);
        return this;
    }

    public ProductDTObuilder setManufactory(String manufactory) {
        this.productDTO.setManufactory(manufactory);
        return this;
    }

    public ProductDTObuilder setReleaseDate(LocalDate releaseDate) {
        this.productDTO.setReleaseDate(releaseDate);
        return this;
    }

    public ProductDTObuilder setWarranty(Integer warranty) {
        this.productDTO.setWarranty(warranty);
        return this;
    }

    public ProductDTObuilder setShopbasket(ShopBasket shopbasket) {
        this.productDTO.setShopbasket(shopbasket);
        return this;
    }

    public ProductDTObuilder setRpm(Long rpm) {
        this.productDTO.setRpm(rpm);
        return this;
    }

    public ProductDTObuilder setWireless(Boolean wireless) {
        this.productDTO.setWireless(wireless);
        return this;
    }

    public ProductDTO build(){
        return this.productDTO;
    }
}
