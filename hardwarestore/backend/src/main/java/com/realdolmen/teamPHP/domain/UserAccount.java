package com.realdolmen.teamPHP.domain;


import javax.persistence.*;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "user_account")
@NamedQueries({
        @NamedQuery( name = UserAccount.FILTER_USERACCOUNT_LIKE,query = "SELECT h FROM UserAccount h WHERE h.name LIKE CONCAT('%',:userAcount,'%')")

}
)
public class UserAccount {
    public static final String FILTER_USERACCOUNT_LIKE = "userNameLike";


    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @OneToOne
    @JoinColumn(name = "login_fk",nullable = true)
    private Login login;

    @OneToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "basket_id", nullable = false)
    private ShopBasket shopbasket;



    //constructors moeten leeg normaal gezien
    public UserAccount() {

    }

    public UserAccount(String name, Login login) {
        this.name = name;
        this.login = login;
    }

    public ShopBasket getShopbasket() {
        return shopbasket;
    }

    public void setShopbasket(ShopBasket shopbasket) {
        this.shopbasket = shopbasket;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login=" + login +
                ", shopbasket=" + shopbasket +
                '}';
    }
}
