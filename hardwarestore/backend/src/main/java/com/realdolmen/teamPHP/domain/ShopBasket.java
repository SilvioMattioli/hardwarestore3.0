package com.realdolmen.teamPHP.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "shopbasket")
public class ShopBasket {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "shopbasket",
            cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Product> productI;

    public ShopBasket() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Product> getProductI() {
        return productI;
    }

    public void setProductI(List<Product> productI) {
        this.productI = productI;
    }

    public void updateBasket(){}
    public void deletItems(){}


}
