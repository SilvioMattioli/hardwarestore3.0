package com.realdolmen.teamPHP.domain;


import javax.persistence.Entity;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "screen")@NamedQueries({
        @NamedQuery( name = Screen.FILTER_PRICE_ABOVE_NUMBER_SCREEN,query = "SELECT s FROM Screen s WHERE s.price > :priceScreen")
})
public class Screen extends Product {

    public static final String FILTER_PRICE_ABOVE_NUMBER_SCREEN = "findprice";


    public Screen() {
    }


}
