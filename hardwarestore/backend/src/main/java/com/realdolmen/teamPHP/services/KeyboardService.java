package com.realdolmen.teamPHP.services;

import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;

import javax.inject.Inject;
import java.util.List;

public class KeyboardService {
    
    @Inject
    private KeyboardRepo keyboardRepo;


    public KeyboardService() {

    }

    public List<Keyboard>findAllKeyBService(){return keyboardRepo.findAll();}
    public Keyboard findKeyboardOnId(Long id){
        return keyboardRepo.findById(id);
    }

    public List<Keyboard>filterOnWirelessService(Boolean i){return keyboardRepo.filterOnWirless(i);}

    public List<Keyboard>filterPriceBtweenService(double priceMin, double priceMax){
        return keyboardRepo.filterPriceBtween(priceMin,priceMax);
    }
    public List<Keyboard> filterManufactuaryService(String manuFacts) {
        return keyboardRepo.filterManufactuary(manuFacts);
    }

    public List<Keyboard> filterPriceAndWirelessService(double priceMin, double priceMax,Boolean wireless) {
        return keyboardRepo.filterPriceAndWireless(priceMin,priceMax,wireless);
    }
    public List<Keyboard> filterNameLikeService(String NameLike) {
        return keyboardRepo.filterNameLike(NameLike);
    }

}