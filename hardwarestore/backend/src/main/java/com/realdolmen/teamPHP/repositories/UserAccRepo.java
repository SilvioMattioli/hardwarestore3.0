package com.realdolmen.teamPHP.repositories;

        import com.realdolmen.teamPHP.domain.UserAccount;

        import javax.persistence.EntityManager;
        import java.util.List;

public class UserAccRepo extends AbstractRepoProduct<UserAccount,Long> {

    public UserAccRepo() {
        super(UserAccount.class);
    }

    public UserAccRepo(EntityManager em) {
        super(em, UserAccount.class);
    }
    
    
    public List<UserAccount> filterNameLike(String NameLike) {
        return em.createNamedQuery(UserAccount.FILTER_USERACCOUNT_LIKE, UserAccount.class)
                .setParameter("userAcount", NameLike)
                .getResultList();
    }




}
