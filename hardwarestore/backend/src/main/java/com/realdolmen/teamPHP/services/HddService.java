package com.realdolmen.teamPHP.services;

import com.realdolmen.teamPHP.domain.HDD;
import com.realdolmen.teamPHP.repositories.HddRepo;

import javax.inject.Inject;
import java.util.List;

public class HddService {
    @Inject
    private HddRepo hddRepo;

    public HddService() {
    }

    public List<HDD> findAllHddService(){return  hddRepo.findAll();}


    public List<HDD>filterPriceAboveHDDService(double priceMin){
        return hddRepo.filterOnPrice(priceMin);
    }
    public List<HDD> filterRpmHDDService(Long rpm) {
        return hddRepo.filterOnRpm(rpm);
    }


}
