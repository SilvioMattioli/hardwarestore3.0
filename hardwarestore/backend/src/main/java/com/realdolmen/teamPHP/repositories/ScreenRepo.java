package com.realdolmen.teamPHP.repositories;


import com.realdolmen.teamPHP.domain.Screen;

import javax.persistence.EntityManager;
import java.util.List;



   public class ScreenRepo extends AbstractRepoProduct<Screen, Long> {


        public ScreenRepo() {
            super(Screen.class);
        }

    public ScreenRepo(EntityManager em) {
        super(em, Screen.class);
    }
        
        
        
       public List<Screen> filterOnPrice(double i){
           return em.createNamedQuery(Screen.FILTER_PRICE_ABOVE_NUMBER_SCREEN,Screen.class)
                   .setParameter("priceScreen",i)
                   .getResultList();
       }




    }

